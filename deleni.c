#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void printStr ( char * str, int * arr, size_t size );
void split ( char * str, int offset, size_t len, int * arr);

int main ( void )
{
	char * str = NULL;
	size_t size;
	getline  ( &str, &size, stdin );
	str[strlen(str)-1] = '\0';
	
	int * arr = (int *) calloc ( strlen(str) * 2 , sizeof ( int ) );
	
	split ( str, 0, strlen(str), arr );
	
	free ( str );
	return 0;
}

void split ( char * str, int offset, size_t len, int * arr)
{
	if ( (size_t) offset == len )
	{
		printStr ( str, arr, len ); 
	}
	
	for ( size_t i = offset; i < len; ++i )
	{
		arr[i]  = 1;	
		split ( str, i + 1, len, arr ); 
		arr[i] = 0;
	}
}

void printStr ( char * str, int * arr, size_t size )
{
	for ( size_t i = 0; i < size; ++i )
	{
		printf ("%c", str[i]);
		if ( arr[i] )
			printf (" ");
	}
	printf ("\n");
}